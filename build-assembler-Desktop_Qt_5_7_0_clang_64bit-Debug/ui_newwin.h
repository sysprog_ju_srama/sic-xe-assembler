/********************************************************************************
** Form generated from reading UI file 'newwin.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWWIN_H
#define UI_NEWWIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_newwin
{
public:
    QWidget *centralwidget;
    QLabel *label;
    QTextEdit *textEdit;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *newwin)
    {
        if (newwin->objectName().isEmpty())
            newwin->setObjectName(QStringLiteral("newwin"));
        newwin->resize(405, 358);
        centralwidget = new QWidget(newwin);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(120, 0, 101, 51));
        textEdit = new QTextEdit(centralwidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(10, 50, 391, 211));
        newwin->setCentralWidget(centralwidget);
        menubar = new QMenuBar(newwin);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 405, 25));
        newwin->setMenuBar(menubar);
        statusbar = new QStatusBar(newwin);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        newwin->setStatusBar(statusbar);

        retranslateUi(newwin);

        QMetaObject::connectSlotsByName(newwin);
    } // setupUi

    void retranslateUi(QMainWindow *newwin)
    {
        newwin->setWindowTitle(QApplication::translate("newwin", "MainWindow", 0));
        label->setText(QApplication::translate("newwin", "OBJECT  CODE", 0));
    } // retranslateUi

};

namespace Ui {
    class newwin: public Ui_newwin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWWIN_H
