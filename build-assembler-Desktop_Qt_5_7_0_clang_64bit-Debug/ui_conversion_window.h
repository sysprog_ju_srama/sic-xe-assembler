/********************************************************************************
** Form generated from reading UI file 'conversion_window.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONVERSION_WINDOW_H
#define UI_CONVERSION_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_conversion_window
{
public:
    QWidget *centralwidget;
    QLabel *label;
    QTextEdit *textEdit;
    QPushButton *pushButton;
    QTextEdit *textEdit_2;
    QPushButton *pushButton_2;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *conversion_window)
    {
        if (conversion_window->objectName().isEmpty())
            conversion_window->setObjectName(QStringLiteral("conversion_window"));
        conversion_window->resize(503, 307);
        centralwidget = new QWidget(conversion_window);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 461, 20));
        textEdit = new QTextEdit(centralwidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(70, 50, 211, 21));
        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(310, 50, 99, 27));
        textEdit_2 = new QTextEdit(centralwidget);
        textEdit_2->setObjectName(QStringLiteral("textEdit_2"));
        textEdit_2->setGeometry(QRect(70, 110, 211, 21));
        pushButton_2 = new QPushButton(centralwidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(310, 110, 99, 27));
        conversion_window->setCentralWidget(centralwidget);
        menubar = new QMenuBar(conversion_window);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 503, 25));
        conversion_window->setMenuBar(menubar);
        statusbar = new QStatusBar(conversion_window);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        conversion_window->setStatusBar(statusbar);

        retranslateUi(conversion_window);

        QMetaObject::connectSlotsByName(conversion_window);
    } // setupUi

    void retranslateUi(QMainWindow *conversion_window)
    {
        conversion_window->setWindowTitle(QApplication::translate("conversion_window", "MainWindow", 0));
        label->setText(QApplication::translate("conversion_window", "                   CONVERSION FROM HEX TO DECIMAL AND VICE VERSA", 0));
        pushButton->setText(QApplication::translate("conversion_window", "TO DECIMAL", 0));
        pushButton_2->setText(QApplication::translate("conversion_window", "TO HEX", 0));
    } // retranslateUi

};

namespace Ui {
    class conversion_window: public Ui_conversion_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONVERSION_WINDOW_H
