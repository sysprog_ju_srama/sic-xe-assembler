/********************************************************************************
** Form generated from reading UI file 'assem.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ASSEM_H
#define UI_ASSEM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_assem
{
public:
    QWidget *centralWidget;
    QTextEdit *textEdit;
    QPushButton *BUILD;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QLabel *label;
    QTextEdit *textEdit_2;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QTextEdit *textEdit_3;
    QTextEdit *textEdit_4;
    QTextEdit *textEdit_5;
    QTextEdit *textEdit_6;
    QTextEdit *textEdit_7;
    QTextEdit *textEdit_8;
    QTextEdit *textEdit_9;
    QLabel *label_11;
    QTextEdit *textEdit_10;
    QLabel *label_12;
    QTextEdit *textEdit_11;
    QTextEdit *textEdit_12;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QListWidget *listWidget;
    QTableView *tableView;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QLabel *label_13;
    QTextEdit *textEdit_13;
    QMenuBar *menuBar;
    QMenu *menuASSEMBLER;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *assem)
    {
        if (assem->objectName().isEmpty())
            assem->setObjectName(QStringLiteral("assem"));
        assem->resize(682, 704);
        centralWidget = new QWidget(assem);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(310, 70, 221, 501));
        BUILD = new QPushButton(centralWidget);
        BUILD->setObjectName(QStringLiteral("BUILD"));
        BUILD->setGeometry(QRect(550, 110, 99, 27));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(550, 160, 99, 27));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(560, 340, 99, 27));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(60, 550, 131, 31));
        textEdit_2 = new QTextEdit(centralWidget);
        textEdit_2->setObjectName(QStringLiteral("textEdit_2"));
        textEdit_2->setGeometry(QRect(20, 600, 631, 81));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(350, 50, 151, 20));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(50, 10, 151, 31));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(20, 50, 111, 20));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(40, 80, 68, 17));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(40, 100, 68, 17));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(40, 120, 68, 17));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(40, 180, 68, 17));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(40, 160, 68, 17));
        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(40, 140, 68, 17));
        textEdit_3 = new QTextEdit(centralWidget);
        textEdit_3->setObjectName(QStringLiteral("textEdit_3"));
        textEdit_3->setGeometry(QRect(130, 50, 104, 16));
        textEdit_4 = new QTextEdit(centralWidget);
        textEdit_4->setObjectName(QStringLiteral("textEdit_4"));
        textEdit_4->setGeometry(QRect(130, 80, 104, 16));
        textEdit_5 = new QTextEdit(centralWidget);
        textEdit_5->setObjectName(QStringLiteral("textEdit_5"));
        textEdit_5->setGeometry(QRect(130, 100, 104, 16));
        textEdit_6 = new QTextEdit(centralWidget);
        textEdit_6->setObjectName(QStringLiteral("textEdit_6"));
        textEdit_6->setGeometry(QRect(130, 120, 104, 16));
        textEdit_7 = new QTextEdit(centralWidget);
        textEdit_7->setObjectName(QStringLiteral("textEdit_7"));
        textEdit_7->setGeometry(QRect(130, 140, 104, 16));
        textEdit_8 = new QTextEdit(centralWidget);
        textEdit_8->setObjectName(QStringLiteral("textEdit_8"));
        textEdit_8->setGeometry(QRect(130, 160, 104, 16));
        textEdit_9 = new QTextEdit(centralWidget);
        textEdit_9->setObjectName(QStringLiteral("textEdit_9"));
        textEdit_9->setGeometry(QRect(130, 180, 104, 16));
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(40, 200, 68, 17));
        textEdit_10 = new QTextEdit(centralWidget);
        textEdit_10->setObjectName(QStringLiteral("textEdit_10"));
        textEdit_10->setGeometry(QRect(130, 200, 104, 16));
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(50, 240, 161, 20));
        textEdit_11 = new QTextEdit(centralWidget);
        textEdit_11->setObjectName(QStringLiteral("textEdit_11"));
        textEdit_11->setGeometry(QRect(150, 270, 104, 16));
        textEdit_12 = new QTextEdit(centralWidget);
        textEdit_12->setObjectName(QStringLiteral("textEdit_12"));
        textEdit_12->setGeometry(QRect(50, 300, 211, 151));
        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(8, 260, 131, 27));
        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(520, 10, 99, 27));
        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(610, 380, 51, 27));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(270, 10, 241, 41));
        tableView = new QTableView(centralWidget);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setGeometry(QRect(20, 300, 256, 192));
        pushButton_6 = new QPushButton(centralWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setGeometry(QRect(550, 210, 99, 27));
        pushButton_7 = new QPushButton(centralWidget);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));
        pushButton_7->setGeometry(QRect(550, 280, 99, 27));
        pushButton_8 = new QPushButton(centralWidget);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));
        pushButton_8->setGeometry(QRect(560, 420, 111, 31));
        label_13 = new QLabel(centralWidget);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(580, 470, 67, 17));
        textEdit_13 = new QTextEdit(centralWidget);
        textEdit_13->setObjectName(QStringLiteral("textEdit_13"));
        textEdit_13->setGeometry(QRect(560, 490, 111, 21));
        assem->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(assem);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 682, 25));
        menuASSEMBLER = new QMenu(menuBar);
        menuASSEMBLER->setObjectName(QStringLiteral("menuASSEMBLER"));
        assem->setMenuBar(menuBar);
        mainToolBar = new QToolBar(assem);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        assem->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(assem);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        assem->setStatusBar(statusBar);
        toolBar = new QToolBar(assem);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        assem->addToolBar(Qt::TopToolBarArea, toolBar);

        menuBar->addAction(menuASSEMBLER->menuAction());

        retranslateUi(assem);

        QMetaObject::connectSlotsByName(assem);
    } // setupUi

    void retranslateUi(QMainWindow *assem)
    {
        assem->setWindowTitle(QApplication::translate("assem", "assem", 0));
        BUILD->setText(QApplication::translate("assem", "BUILD", 0));
        pushButton->setText(QApplication::translate("assem", "RUN", 0));
        pushButton_2->setText(QApplication::translate("assem", "TRACE", 0));
        label->setText(QApplication::translate("assem", "MESSAGE BOX", 0));
        label_2->setText(QApplication::translate("assem", "        EDIT  CODE ", 0));
        label_3->setText(QApplication::translate("assem", "    REGISTER STATUS", 0));
        label_4->setText(QApplication::translate("assem", "ACCUMULATOR", 0));
        label_5->setText(QApplication::translate("assem", "       X", 0));
        label_6->setText(QApplication::translate("assem", "       L", 0));
        label_7->setText(QApplication::translate("assem", "       B", 0));
        label_8->setText(QApplication::translate("assem", "       F", 0));
        label_9->setText(QApplication::translate("assem", "       T", 0));
        label_10->setText(QApplication::translate("assem", "       S", 0));
        label_11->setText(QApplication::translate("assem", "       PC", 0));
        label_12->setText(QApplication::translate("assem", "       MEMORY STATUS", 0));
        pushButton_3->setText(QApplication::translate("assem", "ENTER MEMORY", 0));
        pushButton_4->setText(QApplication::translate("assem", "LOAD FILE", 0));
        pushButton_5->setText(QApplication::translate("assem", "NEXT", 0));
        pushButton_6->setText(QApplication::translate("assem", "Symbol Table", 0));
        pushButton_7->setText(QApplication::translate("assem", "Object Table", 0));
        pushButton_8->setText(QApplication::translate("assem", "Base Conversion", 0));
        label_13->setText(QApplication::translate("assem", "INPUTS", 0));
        menuASSEMBLER->setTitle(QApplication::translate("assem", "ASSEMBLER", 0));
        toolBar->setWindowTitle(QApplication::translate("assem", "toolBar", 0));
    } // retranslateUi

};

namespace Ui {
    class assem: public Ui_assem {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ASSEM_H
