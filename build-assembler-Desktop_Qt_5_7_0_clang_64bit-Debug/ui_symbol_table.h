/********************************************************************************
** Form generated from reading UI file 'symbol_table.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SYMBOL_TABLE_H
#define UI_SYMBOL_TABLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_symbol_table
{
public:
    QWidget *centralwidget;
    QLabel *label;
    QTextEdit *textEdit;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *symbol_table)
    {
        if (symbol_table->objectName().isEmpty())
            symbol_table->setObjectName(QStringLiteral("symbol_table"));
        symbol_table->resize(415, 347);
        centralwidget = new QWidget(symbol_table);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(90, 10, 181, 61));
        textEdit = new QTextEdit(centralwidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(40, 80, 331, 211));
        symbol_table->setCentralWidget(centralwidget);
        menubar = new QMenuBar(symbol_table);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 415, 25));
        symbol_table->setMenuBar(menubar);
        statusbar = new QStatusBar(symbol_table);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        symbol_table->setStatusBar(statusbar);

        retranslateUi(symbol_table);

        QMetaObject::connectSlotsByName(symbol_table);
    } // setupUi

    void retranslateUi(QMainWindow *symbol_table)
    {
        symbol_table->setWindowTitle(QApplication::translate("symbol_table", "MainWindow", 0));
        label->setText(QApplication::translate("symbol_table", "           SYMBOL TABLE", 0));
    } // retranslateUi

};

namespace Ui {
    class symbol_table: public Ui_symbol_table {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SYMBOL_TABLE_H
