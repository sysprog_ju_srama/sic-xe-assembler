/********************************************************************************
** Form generated from reading UI file 'input_window.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INPUT_WINDOW_H
#define UI_INPUT_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_input_window
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QTextEdit *textEdit;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *input_window)
    {
        if (input_window->objectName().isEmpty())
            input_window->setObjectName(QStringLiteral("input_window"));
        input_window->resize(229, 211);
        centralWidget = new QWidget(input_window);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 10, 201, 20));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(50, 40, 104, 41));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(50, 100, 99, 27));
        input_window->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(input_window);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 229, 25));
        input_window->setMenuBar(menuBar);
        mainToolBar = new QToolBar(input_window);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        input_window->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(input_window);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        input_window->setStatusBar(statusBar);

        retranslateUi(input_window);

        QMetaObject::connectSlotsByName(input_window);
    } // setupUi

    void retranslateUi(QMainWindow *input_window)
    {
        input_window->setWindowTitle(QApplication::translate("input_window", "input_window", 0));
        label->setText(QApplication::translate("input_window", "     READ   INPUT IN INTEGER", 0));
        pushButton->setText(QApplication::translate("input_window", "ENTER", 0));
    } // retranslateUi

};

namespace Ui {
    class input_window: public Ui_input_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INPUT_WINDOW_H
