/****************************************************************************
** Meta object code from reading C++ file 'assem.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../ass/assem.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'assem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_assem_t {
    QByteArrayData data[30];
    char stringdata0[416];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_assem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_assem_t qt_meta_stringdata_assem = {
    {
QT_MOC_LITERAL(0, 0, 5), // "assem"
QT_MOC_LITERAL(1, 6, 16), // "on_BUILD_clicked"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(4, 46, 8), // "link_gui"
QT_MOC_LITERAL(5, 55, 8), // "find_add"
QT_MOC_LITERAL(6, 64, 5), // "char*"
QT_MOC_LITERAL(7, 70, 8), // "find_val"
QT_MOC_LITERAL(8, 79, 6), // "search"
QT_MOC_LITERAL(9, 86, 6), // "char**"
QT_MOC_LITERAL(10, 93, 7), // "execute"
QT_MOC_LITERAL(11, 101, 4), // "int*"
QT_MOC_LITERAL(12, 106, 8), // "execute1"
QT_MOC_LITERAL(13, 115, 23), // "on_pushButton_3_clicked"
QT_MOC_LITERAL(14, 139, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(15, 163, 23), // "on_pushButton_4_clicked"
QT_MOC_LITERAL(16, 187, 23), // "on_pushButton_5_clicked"
QT_MOC_LITERAL(17, 211, 10), // "mem_status"
QT_MOC_LITERAL(18, 222, 12), // "find_add_opn"
QT_MOC_LITERAL(19, 235, 9), // "clear_reg"
QT_MOC_LITERAL(20, 245, 10), // "reg_status"
QT_MOC_LITERAL(21, 256, 11), // "gen_ob_code"
QT_MOC_LITERAL(22, 268, 22), // "object_code_generation"
QT_MOC_LITERAL(23, 291, 11), // "gen_sym_tab"
QT_MOC_LITERAL(24, 303, 23), // "on_pushButton_6_clicked"
QT_MOC_LITERAL(25, 327, 23), // "on_pushButton_7_clicked"
QT_MOC_LITERAL(26, 351, 12), // "error_handle"
QT_MOC_LITERAL(27, 364, 23), // "on_pushButton_8_clicked"
QT_MOC_LITERAL(28, 388, 10), // "take_input"
QT_MOC_LITERAL(29, 399, 16) // "check_if_integer"

    },
    "assem\0on_BUILD_clicked\0\0on_pushButton_clicked\0"
    "link_gui\0find_add\0char*\0find_val\0"
    "search\0char**\0execute\0int*\0execute1\0"
    "on_pushButton_3_clicked\0on_pushButton_2_clicked\0"
    "on_pushButton_4_clicked\0on_pushButton_5_clicked\0"
    "mem_status\0find_add_opn\0clear_reg\0"
    "reg_status\0gen_ob_code\0object_code_generation\0"
    "gen_sym_tab\0on_pushButton_6_clicked\0"
    "on_pushButton_7_clicked\0error_handle\0"
    "on_pushButton_8_clicked\0take_input\0"
    "check_if_integer"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_assem[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  139,    2, 0x08 /* Private */,
       3,    0,  140,    2, 0x08 /* Private */,
       4,    0,  141,    2, 0x08 /* Private */,
       5,    1,  142,    2, 0x08 /* Private */,
       7,    1,  145,    2, 0x08 /* Private */,
       8,    2,  148,    2, 0x08 /* Private */,
      10,    5,  153,    2, 0x08 /* Private */,
      12,    6,  164,    2, 0x08 /* Private */,
      13,    0,  177,    2, 0x08 /* Private */,
      14,    0,  178,    2, 0x08 /* Private */,
      15,    0,  179,    2, 0x08 /* Private */,
      16,    0,  180,    2, 0x08 /* Private */,
      17,    0,  181,    2, 0x08 /* Private */,
      18,    1,  182,    2, 0x08 /* Private */,
      19,    0,  185,    2, 0x08 /* Private */,
      20,    0,  186,    2, 0x08 /* Private */,
      21,    0,  187,    2, 0x08 /* Private */,
      22,    0,  188,    2, 0x08 /* Private */,
      23,    0,  189,    2, 0x08 /* Private */,
      24,    0,  190,    2, 0x08 /* Private */,
      25,    0,  191,    2, 0x08 /* Private */,
      26,    0,  192,    2, 0x08 /* Private */,
      27,    0,  193,    2, 0x08 /* Private */,
      28,    0,  194,    2, 0x08 /* Private */,
      29,    1,  195,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Int, 0x80000000 | 6,    2,
    QMetaType::Int, 0x80000000 | 6,    2,
    QMetaType::Int, 0x80000000 | 6, 0x80000000 | 9,    2,    2,
    QMetaType::Void, 0x80000000 | 9, 0x80000000 | 9, QMetaType::Int, 0x80000000 | 9, 0x80000000 | 11,    2,    2,    2,    2,    2,
    QMetaType::Void, 0x80000000 | 9, 0x80000000 | 9, QMetaType::Int, 0x80000000 | 9, 0x80000000 | 11, QMetaType::Int,    2,    2,    2,    2,    2,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Int, 0x80000000 | 6,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool, 0x80000000 | 6,    2,

       0        // eod
};

void assem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        assem *_t = static_cast<assem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_BUILD_clicked(); break;
        case 1: _t->on_pushButton_clicked(); break;
        case 2: _t->link_gui(); break;
        case 3: { int _r = _t->find_add((*reinterpret_cast< char*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 4: { int _r = _t->find_val((*reinterpret_cast< char*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 5: { int _r = _t->search((*reinterpret_cast< char*(*)>(_a[1])),(*reinterpret_cast< char**(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 6: _t->execute((*reinterpret_cast< char**(*)>(_a[1])),(*reinterpret_cast< char**(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< char**(*)>(_a[4])),(*reinterpret_cast< int*(*)>(_a[5]))); break;
        case 7: _t->execute1((*reinterpret_cast< char**(*)>(_a[1])),(*reinterpret_cast< char**(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< char**(*)>(_a[4])),(*reinterpret_cast< int*(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6]))); break;
        case 8: _t->on_pushButton_3_clicked(); break;
        case 9: _t->on_pushButton_2_clicked(); break;
        case 10: _t->on_pushButton_4_clicked(); break;
        case 11: _t->on_pushButton_5_clicked(); break;
        case 12: _t->mem_status(); break;
        case 13: { int _r = _t->find_add_opn((*reinterpret_cast< char*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 14: _t->clear_reg(); break;
        case 15: _t->reg_status(); break;
        case 16: _t->gen_ob_code(); break;
        case 17: _t->object_code_generation(); break;
        case 18: _t->gen_sym_tab(); break;
        case 19: _t->on_pushButton_6_clicked(); break;
        case 20: _t->on_pushButton_7_clicked(); break;
        case 21: _t->error_handle(); break;
        case 22: _t->on_pushButton_8_clicked(); break;
        case 23: _t->take_input(); break;
        case 24: { bool _r = _t->check_if_integer((*reinterpret_cast< char*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObject assem::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_assem.data,
      qt_meta_data_assem,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *assem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *assem::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_assem.stringdata0))
        return static_cast<void*>(const_cast< assem*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int assem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
