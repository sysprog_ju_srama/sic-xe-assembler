================================
ASSEMBLER LAB :
================================

Date: 25th November, 2016

=======
Description : A Basic Simulator for An Assembler which reads and executes SIC code with certain SIC/XE features like Program Counter Relative Indexed Addressing, basic Floating Point arithmetic calculations. We have made a Simple GUI which takes a SIC^^ code as input (in .txt format) and gives the corresponding output in the form of memory status and register status along with the object code, symbol table and list of errors(if any). A decimal to hexadecimal  & vice-versa converter has also been made available by means of a button on GUI through which the required interconversions can be made.


    By :ADITYA AGARWAL (ROLL - 62)

       ANEEK ROY (ROLL - 72)

       MALYABAN BAL(ROLL - 49)

       RAJDEEP SUROLIA (ROLL - 60)

       SAURAJIT CHAKRABORTY (ROLL - 61)

====================================
DEPENDENCIES :
====================================

C++ and Qt( version 5.7.0 ) and Qt Creator. Qt has been used for the GUI, using the open source license.It can be downloaded and installed from https://www.qt.io/download-open-source/.
The file optab.txt should be present in the build folder, for the program to work.

====================================
DETAILED OVERVIEW :
====================================

1) Given a Load browser we can load any file( .txt file containing the SIC^^ code) in the editor given in the GUI.

2) The code from the text-edit section is then copied to the data file to be loaded as input to the assembler.

3) The BUILD button is to check if there are any errors in the program. If any, the error and corresponding line number is given in the message box If not then the message "code built successfully" is shown. The error_handle() function takes care of it.

	a) If there are errors the corresponding buttons of RUN,TRACE,OPCODE and SYMBOL_TABLE are disabled.All the registers and memory is cleared.

4) If there are no errors the user can press the RUN button and it will displays the contents of all the registers and the user can even go to any memory location using the ENTER MEMORY button.

5) The INPUTS text-box takes any input given by the user (corresponding to any RD call) and stores it in a integer queue.

6) Each and every line of the program can be separating executed using the DEBUG and NEXT buttons.

7) The entire memory and its contents are shown in the UI in the lower left hand side of the window, where any memory location can be randomly accessed using the "ENTER MEMORY" button and text-field provided. Scrolling capability has been provided as well.


Note: SIC^^ refers to functionality of SIC and certain extra functionalities of SIC/XE like, logical and arithmetic operations between registers, program counter relative addressing, basic floating point operations between registers.

=======================================
SOURCE FILE DESCRIPTIONS
=======================================

a) newwin.h and newwin.cpp
--------------------------

It is used to display the object code of our SIC^^ code in a separate window and link it with our original main window.

b) symbol_table.h and symbol_table.cpp:
---------------------------------------

It is used to display the symbol table containing the various labels and their corresponding addresses in the Program in a separate window and link it with our original main window.


c) coversion_window.h and conversion_window.cpp:
------------------------------------------------

It is mainly used for base conversion. A new window opens and any decimal number given by the user can be converted to hexadecimal and vice versa.


d) Memory.cpp:
-------------------

A Memory class has been created where the memory has been encapsulated as an array of bytes. Its default size is of 2^20 bytes (SIC/XE standard) but can be changed during compile-time if the programmer so wishes. Storing and loading of word(s) and byte(s) has been made available to the user by means of functions, like store(Word/Byte) (location, content), load(Word/Byte)(location).


e) assem.h and assem.cpp :
---------------------------

They define the basic operations of the assembler. The execution and building details are incorporated in assem.cpp.assem.h contains the corresponding function declarations.


f) hexDecInterconversion.cpp :
------------------------------

A class has been designed which helps in the conversion. The class takes in either a binary/dec/hex number and converts it to the other two bases. The conversion is performed as follows. The number is input in either of these 3 bases. Then the object calls the convert() function. In that function, the number is converted to binary (if not input in binary) by means of the function make_binary(), and then converted to the other base(s) by means of functions make_hex() & make_dec(). The converted numbers are accessible by means of setter & getter functions.


g) arithmetic.h :
------------------

The header file contains all the backend functionalities of the arithmetic and logical functions to handle calls made by the execute() function in assem.cpp. Contains fucntions like TIXR, ADD, SUB, JLT, JEQ, COMP, COMPR, etc.


================================================
FILES AND OTHER NOTES
================================================

In the directory in which the project is built we have also kept all our files in a separate directory (buildAssembler desktop ) which is accessed by Qt while building our code. The files include:

input.txt, input1.txt, input2.txt and input3.txt (input files), optab.txt, error_file.txt, data.txt, symtab_final.txt, object.txt are all contained in that folder.

On opening Qt and loading our project assembler if we simulate our assembler by executing the file assem.cpp .

Data.txt present in the build folder, should be deleted whenever the code does not "BUILD" successfully in the GUI.


==================================================
ASSEMBLER DETAILS:
==================================================

The execution model is based on a two pass assembler. In the first pass we resolves issues such as forward references and in the second pass we generate the object code. The execution function helps in execution of the program. We also introduce a debugging module which helps us to trace line by line and see if the program encounters any error. The register and memory contents are changed accordingly. The addressing mode is primarily Direct addressing. We have used indexed addressing when using a string or an array.The instructions defined along with their opcodes are kept in the optab.txt file. The sysbol table is generated in the symtab_final.txt. The object code is generated in object.txt. Along with that we have added additional GUI facilities which can also be used to explore the above mentioned files in a read-only fashion. The new instructions such as ADDR,SUBR uses a modified form of addressing.	The address field gives information about the two registers used. The error handling module efficiently handles errors such as label mismatch, unknown instruction etc.


==================================================
CONTRIBUTIONS:
==================================================

ADITYA AGARWAL : Built modules such as Memory Handling and Mapping, Error_Handling, Tracing, taking  inputs from User, and conversion from Hexadecimal to Decimal and vice versa and arithmetic functions like LDA, STA, etc and helped in linking assembler with the GUI.

SAURAJIT CHAKRABORTY : Built the basic structure of GUI with all the buttons and text boxes. Modules such as Error_Handling , Debugging, taking inputs from User, newwin.cpp, symbol_table.cpp, symbol table generation and conversion_window.cpp and linked the assembler with the GUI and arithmetic functions like JLT, JGT etc.

MALYABAN BAL : Built the basic model of the assembler. Built the modules such as object_Code_generation, tracing , execution of SIC functions, logic of arithmetic functions like TIXR, ADDR, SUBR, COMPR, OR, LDA, STA, etc and memory mapping and also helped in linking assembler with the GUI.

ANNEK ROY :Built basic floating point arithmetic functions and register initialisation and register status and subroutine functions like JSUB,RSUB ETC.

RAJDEEP SUROLIA: Built file handling functions like open_all_files() called in link::gui() and arithmetic functions like MUL,DIV,ADD,SUB,SHIFTL etc
