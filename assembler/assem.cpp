/*-------------------------------headers----------------------------------*/
#include "assem.h"
#include "ui_assem.h"

#include<iostream>
#include<fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include<string>
#include <map>
#include <QFile>
#include <QTextStream>
#include<QFileDialog>
#include<limits>
#include<QStandardItemModel>
#include<QMessageBox>
#include <QTableWidgetItem>
#include <QTableWidget>
#include "newwin.h"
#include "symbol_table.h"
#include "input_window.h"
#include "conversion_window.h"

#include "arithmetic.h"
#include "Memory.cpp"
#include <QTimer>
#include<queue>
#include<algorithm>

/*-------------------------global declarations----------------------------------------*/
using namespace std;
int register1[10]={0,0,0,0,0,0,0,0,0,0};
const char INPUT_FILE_NAME[] = "Data.txt";
const char ERROR_FILE_NAME[] = "error_file.txt";
int position = 1;int count=0;
int lc,sa;
char name[10],opnd[10],la[10],mne[10],s1[10],mne1[10],opnd1[10];
char lcs[10],ms[10];
char sym[10],symaddr[10],s2[10],q[10],s3[10];

queue<int> q_rd;
map<char, int> reg_resolution;
Memory memory;
int store_lc,store_final_lc;
int flag = 0,flag_lc=0;
int vals_list[100];
int indu=0;
int length1;
char** func_list;
char** opn_list;
FILE *f1,*f2,*f3,*f4,*f5,*f6,*f7,*f8;
int error_flag = 0;
char obj1[10],obj2[10];int len1;
int pgmlen;

float float_c, float_b, float_acc = 0.0f;

char** proc12;
char** operand12;
int value_list1[100];
int final_pc;


/*--------------------------------take input from text box------------------------------------------*/
void assem::take_input(){

    QString p;
    QString h = ui->textEdit_13->toPlainText();
    string s = h.toStdString();
    h = h+" ";
    s = s+" ";
    int sum=0;
    for(int j=0;j<s.size();j++){

        if(s[j]!=' ' && s[j] <= '9' && s[j] >= '0') {
            sum = 10*sum + (s[j]-48);
        }
        else if(s[j] != ' '){
            sum = s[j];
        }
        else{
            q_rd.push(sum);
            //q_temp.push(sum);
            sum=0;
        }
    }
}

/*---------------------------------------gui constructor----------------------------------*/
assem::assem(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::assem)
{
    ui->setupUi(this);
    ui->pushButton_5->setEnabled(false);

}

/*------------------------------------------gui destructor----------------------------------*/
assem::~assem()
{
    delete ui;
}

/*--------------------------------------on clicking the button build the text from file gets stored in the text box--------*/
void assem::on_BUILD_clicked()
{

    QString text = ui->textEdit->toPlainText();
    /*copying the code from the edit section to the data file to be loaded as input to the assembler*/
    QString filename = INPUT_FILE_NAME;                      // "Data.txt";
        QFile file(filename);
        if (file.open(QIODevice::ReadWrite)) {
            QTextStream stream(&file);
            stream << text << endl;
        }
    /*to check if the code has any errors or not*/

        error_flag = 0;
        ui->textEdit_2->clear();
        error_handle();
        if(error_flag==0){
             ui->pushButton->setEnabled(true);
             ui->pushButton_6->setEnabled(true);
             ui->pushButton_7->setEnabled(true);
             ui->pushButton_2->setEnabled(true);
             ui->textEdit_2->setText("code built successfully !!");
         }
        else{
            ui->pushButton->setEnabled(false);
            ui->pushButton_6->setEnabled(false);
            ui->pushButton_7->setEnabled(false);
            ui->pushButton_2->setEnabled(false);
            QFile file(ERROR_FILE_NAME);
            file.open(QIODevice::ReadOnly);
            QTextStream stream(&file);
            QString content = stream.readAll();
            file.close();
            ui->textEdit_2->setText(content);
            clear_reg();
            reg_status();

        }


}



/*----------------------to display register status-------------------------------------*/
void assem::reg_status(){
    /*displaying the contents of the register*/
    ui->textEdit_3->setText(QString::number(register1[ reg_resolution['A'] ]));
    ui->textEdit_4->setText(QString::number(register1[ reg_resolution['X'] ]));
    ui->textEdit_5->setText(QString::number(register1[ reg_resolution['L'] ]));
    ui->textEdit_6->setText(QString::number(register1[ reg_resolution['B'] ]));
    ui->textEdit_7->setText(QString::number(register1[ reg_resolution['S'] ]));
    ui->textEdit_8->setText(QString::number(register1[ reg_resolution['T'] ]));
    ui->textEdit_9->setText(QString::number(float_acc));     //register1[ reg_resolution['F'] ]
    ui->textEdit_10->setText(QString::number(register1[ reg_resolution['P'] ]));
}



/*----------------------to generate object code-----------------------------------------*/

void assem::gen_ob_code(){

    fclose(f7);
    QFile file("object.txt");
    file.open(QIODevice::ReadOnly);
    QTextStream stream(&file);
    QString content = stream.readAll();
    file.close();
    uip = new  newwin(this);
    uip->show();
    uip->getstring(content);

}


/*-------------------generate symbol table-------------------------------------------*/

void assem::gen_sym_tab(){

    fclose(f4);
    QFile file("symtab_final.txt");
    file.open(QIODevice::ReadOnly);
    QTextStream stream(&file);
    QString content = stream.readAll();
    file.close();
    uiz = new  symbol_table(this);
    uiz->show();
    uiz->getstring1(content);
}


/*-----------on clicking the button run the register status,memory status,symbol table and object codegets genereted---*/
void assem::on_pushButton_clicked()
{
    /*-------------to allow for multiple runs------------------------------*/
    error_flag = 0;
    position = 1;
    ::count = 0;
    flag = 0;
    indu = 0;
    ui->textEdit_2->clear();

    /*checking for errors*/
    //error_handle();
    if(error_flag==0){
        take_input();
        link_gui();
        printf("%d is final pc\n",final_pc);
        register1[reg_resolution['P']]=final_pc;
        reg_status();
        mem_status();
    }

}

/*-----------------to check if the operand is an integer or not for error handling--------------------------------------*/
bool assem::check_if_integer(char* s){

    int at = atoi(s);
    if(at)
        return true;
    int len = strlen(s);
    for(int i=0;i<len;i++){

        if(s[i]!='0')
            return false;
    }

    return true;
}

/*----------------------error handling module to detect errors in the user code------------------------------------*/

void assem::error_handle(){

    f1 = fopen(INPUT_FILE_NAME,"rw+");
    f8 = fopen("error_file.txt","w+");
    char l[10],op[10],oper[10],opcode[10],fg[10];
    int g=0,count_line = 1;
    char error[] = "opcode_mismatch";
    char error1[] = "label_mismatch";
    char error2[] = "integer_operand_expected";
    map<string,int> m;

    /*for opcode mismatch*/
    while(!feof(f1)){
        g=0;
        f2 = fopen("optab.txt","r");
        fscanf(f1,"%s%s%s",l,op,oper);
        while(!feof(f2)){

            fscanf(f2,"%s%s",opcode,fg);
            if(strcmp(op,opcode)==0 || strcmp(op,"START")==0 || strcmp(op,":")==0 || strcmp(op,"END")==0 ||  strcmp(op,"RESW")==0 ||  strcmp(op,"WORD")==0 ||  strcmp(op,"RESB")==0 ||  strcmp(op,"BYTE")==0){
                g=1;
                break;
               }

        }
        fclose(f2);
        if(g==0){

               error_flag = 1;
               fprintf(f8,"%d\t%s\t\t\"%s\t%s\t%s\"\n",count_line-1,error, l, op, oper);
        }
        count_line++;


    }
    fclose(f1);

    count_line = 1;
    f1 = fopen(INPUT_FILE_NAME,"rw+");
    m["A"]= 1;
    m["X"] = 1;
    m["B"] = 1;
    m["S"] = 1;
    m["T"] = 1;
    m["L"] = 1;
    m["P"] = 1;
    m["F"] = 1;
    m["-"] = 1;

    /*for label mismatch*/
    while(!feof(f1)){

        fscanf(f1,"%s%s%s",l,op,oper);
        if(strcmp(op,"START")==0){
            if(!check_if_integer(oper)){

                error_flag = 3;
                fprintf(f8,"%d\t%s\n",count_line-1,error2);
            }
            else{
                m[string(oper)] = 1;
            }
        }

        else if(strcmp(l,"-")!=0)
            m[string(l)]=1;

    }
    fclose(f1);
    f1 = fopen(INPUT_FILE_NAME,"rw+");
    int position_of_comma = -1;
    while(!feof(f1)){
        position_of_comma = -1;
        count_line++;
        fscanf(f1,"%s%s%s",l,op,oper);
        if(strcmp(op,"END") == 0)
            break;
        else if(strcmp(op,"BYTE")==0 || strcmp(op,"RESB")==0 || strcmp(op,"WORD")==0 || strcmp(op,"RESW")==0){
           // continue;
            if(!check_if_integer(oper)){
                error_flag = 3;
                fprintf(f8,"%d\t%s\n",count_line-1,error2);
            }
        }

        else{
            string str(oper);
            for(int h=0;h<str.size();h++)
                if(str.at(h)==','){
                    position_of_comma = h;
                    break;
                }
            //cout<<position_of_comma<<endl;
            if(position_of_comma==-1)
                if(m[str]!=1){
                    error_flag = 2;
                    fprintf(f8,"%d\t%s\t\t\"%s\t%s\t%s\"\n",count_line-1,error1, l, op, oper);
                }

                else{
                    string first = str.substr(0,position_of_comma);

                    string second = str.substr(position_of_comma+1);
                    if(m[first]!=1 || m[second]!=1){
                        error_flag = 2;
                        fprintf(f8,"%d\t%s\t\t%s\t%s\t%s\n",count_line-1,error1, l, op, oper);
                    }

                }

        }
     }
    fclose(f1);
    fclose(f8);


}


/*-------------------------------to display contents of memory------------------------------------------------*/
void assem::mem_status(){

    QStandardItemModel *model = new QStandardItemModel(104567,2,this); //2 Rows and 2 Columns
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("MEMORY")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("VALUE")));
    ui->tableView->setModel(model);


    int col =0;
    for(int row = 0; row < 104957; row++)
        {

                QModelIndex index = model->index(row,col,QModelIndex());
                model->setData(index,row);

        }

   col = 1;
   for(int row = 0; row < 104957; row++)
   {

                   QModelIndex index = model->index(row,col,QModelIndex());
                   model->setData(index,memory.loadByte(row));

   }

  // ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

}



/*---------------------for   displaying any particular memory location----------------------------------------------*/

void assem::on_pushButton_3_clicked()
{

    if(error_flag!=0)
            ui->textEdit_2->setText("compile error, not possible to run");
    else{
        QString text1 = ui->textEdit_11->toPlainText();
        int p = text1.split(" ")[0].toInt();
        if(p>104857)
                ui->textEdit_2->setText("error out of memory :");
        else{


            ui->tableView->selectRow(p);
            ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

        }
    }
}



/*-------------------------initialization of map of registers--------------------------------------*/
void reg_res_init(void){
    reg_resolution['A'] = 0;
    reg_resolution['X'] = 1;
    reg_resolution['L'] = 2;
    reg_resolution['B'] = 3;
    reg_resolution['S'] = 4;
    reg_resolution['T'] = 5;
    reg_resolution['F'] = 6;		//implemented using double
    reg_resolution['P'] = 8;		//P == PC
    reg_resolution['W'] = 9;		//W == SW
}



/*------------------------------opening all required files-------------------------------------------------*/

void open_all_files(){
    f1=fopen(INPUT_FILE_NAME, "rw+");
    f2=fopen("optab.txt","r");
    f3=fopen("symtab.txt","w+");
    f4=fopen("symtab_final.txt","w+");
    f5=fopen("output.txt","w+");
    f6 = fopen(INPUT_FILE_NAME,"rw+");
    f7 = fopen("object.txt","w+");
}




/*------------------------------------------------------------------*/

int assem::find_add(char *s) {
    int i;
    for(i=0;i<indu;i++) {
        if(opn_list[i]!=NULL && strcmp(opn_list[i],s)==0) {
            return i;
        }
    }

    return -1;
}



/*-------------------------------to find a given label in the corresponding SIC code--------------------*/
int assem::find_val(char *s) {


    f1 = fopen(INPUT_FILE_NAME,"rw+");
    char a[10],b[10],c[10];
    fscanf(f1,"%s%s%s",a,b,c);
    while(!feof(f1) && strcmp(a,s)!=0) {
            fscanf(f1,"%s%s%s",a,b,c);

     }
    fclose(f1);
    int p=-1;
    p=find_add(s);
    if(p>=0) {
        if(strcmp(b,"WORD")==0)
        memory.storeWord(vals_list[p], atoi(c));
        return memory.loadWord(vals_list[p]);
    }
    //fclose(f1);

    return atoi(c);
}



/*----------------------------------------to search for a label in the symtab file------------------------------*/

int assem::search(char *p,char** func_list) {
    int i=0;
    while(strcmp(p,func_list[i])!=0){
        i++;
    }
    return i;
}



/*-------------------------------------------executing various functions-----------------------------------------*/
void assem::execute(char** proc,char** operand,int length,char** func_list,int* value_list) {

    int i=0;int state = 1;
    for(i=0;i<10;i++) value_list[i]=value_list1[i];
    i=0;
    while(i<length) {
        printf("EXECUTED CODES :    %s  %s\n",proc[i],operand[i]);
        if(strcmp(proc[i],"MOD")==0) {
            MOD(register1[ reg_resolution['A'] ], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"RD")==0) {
            int v=0;

           if(q_rd.empty()==false){
                v = q_rd.front();
                q_rd.pop();
            }
            /*   while(q_temp.size()){
                    cout<<"VALUE IS "<<q_temp.front()<<"\n";
                    q_temp.pop();
                }    */

            RD(v,register1[ reg_resolution['A'] ]);

        }
        else if(strcmp(proc[i],"WD")==0) {
            //to be implemented
        }

        /*new functions*/

        else if(strncmp(proc[i], "ST", 2)==0) {     //contains ST(reg) and STCH(as well)
                    int loop=0;
                    int split=0;
                    char name[20];
                    for(loop=0;operand[i][loop]!='\0';loop++) {

                        if(operand[i][loop]==','){

                            name[loop]='\0';
                            split=1;
                            break;
                        }
                        name[loop]=operand[i][loop];
                    }
                    name[loop]='\0';
                    if(split==0) {
                        int opera;
                        opera = find_add(operand[i]);
                        int reg_value;
                        if(strcmp(proc[i], "STCH") != 0){
                            ST( register1[ reg_resolution[proc[i][2]] ], reg_value );
                            memory.storeWord(vals_list[opera], reg_value);
                        }
                        else{
                            ST( register1[ reg_resolution['A'] ], reg_value );
                            memory.storeByte(vals_list[opera], reg_value);
                        }
                    }
                    else {
                        int opera=find_add(name);
                        int reg_value, memloc;
                        //memloc = vals_list[opera]+ 3*register1[ reg_resolution['X'] ];
//                      cout <<"vallsit->" << vals_list[opera] << "   ||||    " << register1[ reg_resolution['X'] ] <<"\t";
//                      cout<<"MEMORY : _________________"<<memloc<<"\n";
                        if(strcmp(proc[i], "STCH") != 0){
			                memloc = vals_list[opera]+register1[ reg_resolution['X'] ]*3;
                            ST( register1[ reg_resolution[proc[i][2]] ], reg_value );
                            memory.storeWord(memloc, reg_value);
                        }
                        else{
			                memloc = vals_list[opera]+register1[ reg_resolution['X'] ];
                            ST( register1[ reg_resolution['A'] ], reg_value );
                            memory.storeByte(memloc, reg_value);
                        }
                    }
        }
        else if(strncmp(proc[i], "LD", 2) == 0) {   //contains LD(reg) and LDCH(as well)
                    int loop=0;
                    int split=0;
                    char name[20];
                    for(loop=0;operand[i][loop]!='\0';loop++){
                        if(operand[i][loop]==','){
                            name[loop]='\0';
                            split=1;
                            break;
                        }
                        name[loop]=operand[i][loop];
                    }
                    name[loop]='\0';
                    if(split==0) {
                        int opera=find_val(operand[i]);
                //        cout<<"LOADING MEMORY in reg "<< proc[i][2] <<" IS "<< opera <<"\n";
                        if(strcmp(proc[i], "LDCH") != 0){
                            LD(register1[reg_resolution[proc[i][2]]],opera);
                        }
                        else{
                            LD(register1[reg_resolution['A']],(opera & 0xFF) );
                        }

                    }
                    else {
                        int opera=find_add(name);
                        //cout << "\n\nIN LD: proc[i] = "<< proc[i] << " VALS LIST OPERA IS " << vals_list[opera] << " X reg = " << 3*register1[ reg_resolution['X'] ] << "\n";
                        int memloc;// = vals_list[opera] + 3*register1[ reg_resolution['X'] ];
                        //cout<<"LOADING MEMORY in reg "<< proc[i][2] <<"\t";
                        //cout<<"LOADING MEMORY address IS "<< memloc <<"\n";
                        if(strcmp(proc[i], "LDCH") != 0){
		  	                memloc = vals_list[opera] + 3*register1[ reg_resolution['X']];
                            LD(register1[ reg_resolution[proc[i][2]] ], memory.loadWord( memloc ));
                        }
                        else{
                            memloc = vals_list[opera] + register1[ reg_resolution['X'] ];
                            LD(register1[reg_resolution['A']],memory.loadByte(memloc) );
                        }
                    }
        }
	    else if(strcmp(proc[i],"DEC")==0){
            register1[reg_resolution[operand[i][0]]]--;
    //        ADD(register1[reg_resolution['A']], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"INC")==0){
            register1[reg_resolution[operand[i][0]]]++;
    //        ADD(register1[reg_resolution['A']], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"ADD")==0){
            ADD(register1[reg_resolution['A']], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"SUB")==0){
            SUB(register1[reg_resolution['A']], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"SUBR")==0){
            SUB( register1[ reg_resolution[operand[i][2]] ], register1[ reg_resolution[operand[i][0]] ] );
        }
        else if(strcmp(proc[i],"ADDR")==0){
            ADD( register1[ reg_resolution[operand[i][2]] ], register1[ reg_resolution[operand[i][0]] ] );			//ADDR A, S means Add A to S and store it in S.
        }
        else if(strcmp(proc[i],"COMP")==0){
            state = COMP( register1[ reg_resolution['A'] ], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"COMPR")==0){
            state = COMP( register1[ reg_resolution[operand[i][0]] ], register1[ reg_resolution[operand[i][2]] ] );
        }
        else if(strcmp(proc[i],"DIV")==0){
            DIV(register1[reg_resolution['A']], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"DIVR")==0){
            DIV( register1[ reg_resolution[operand[i][2]] ], register1[ reg_resolution[operand[i][0]] ] );
        }
        else if(strcmp(proc[i],"MUL")==0){
            MUL(register1[reg_resolution['A']], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"MULR")==0){
            MUL( register1[ reg_resolution[operand[i][2]] ], register1[ reg_resolution[operand[i][0]] ] );
        }
        else if(strcmp(proc[i],"JLT")==0) {
            int id=0;
            id=search(operand[i],func_list);
            if(state<0)
                i=value_list[id]-1;
                printf("state is %d --- %d jump is i and equi is %d\n",state,value_list[id],i+1);
           //     printf("JUMPS TO %s %s\n",proc[i+1],)
                //	printf("%d is acc\n",register1[0]);
    //            cout << "IN JLT :: " << i << " <-- i ::: id -> :::: " << id <<"\n";
        }
        else if(strcmp(proc[i],"JEQ")==0) {
            int id=0;
            id=search(operand[i],func_list);
            if(state==0)
            i=value_list[id]-1;
            printf("state is %d --- %d jump is i and equi is %d\n",state,value_list[id],i+1);
        }
	else if(strcmp(proc[i],"JMP")==0) {
            int id=0;
            id=search(operand[i],func_list);
        //    if(state>0)
            i=value_list[id]-1;
            printf("state is %d --- %d jump is i and equi is %d\n",state,value_list[id],i+1);
        }
        else if(strcmp(proc[i],"JGT")==0) {
            int id=0;
            id=search(operand[i],func_list);
            if(state>0)
            i=value_list[id]-1;
            printf("state is %d --- %d jump is i and equi is %d\n",state,value_list[id],i+1);
        }
        else if(strcmp(proc[i],"JSUB")==0) {
            int id=0;
            id=search(operand[i],func_list);
            i=value_list[id]-1;
            register1[ reg_resolution['L'] ] = register1[ reg_resolution['P'] ];
            register1[ reg_resolution['P'] ] = find_val(operand[i]);
        }
        else if(strcmp(proc[i],"RSUB")==0) {
            int id=0;
            id=search(operand[i],func_list);
            register1[ reg_resolution['P'] ] = register1[ reg_resolution['L'] ];
        }
        else if(strcmp(proc[i],"TIXR")==0){
            printf("%d is the T reg value\n",register1[ reg_resolution[operand[i][0]] ]);
            state=TIXR(register1[ reg_resolution[operand[i][0]] ], register1[reg_resolution['X']]);
        }
        else if(strcmp(proc[i],"TIX")==0){
            state=TIXR( find_val(operand[i]) , register1[reg_resolution['X']] );
        }
        else if(strcmp(proc[i],"OR")==0){
            OR( register1[reg_resolution['A']],  find_val(operand[i])  );
        }
        else if(strcmp(proc[i],"AND")==0){
            AND( register1[reg_resolution['A']],  find_val(operand[i])  );
        }
        else if(strcmp(proc[i],"CLEAR")==0){
            CLEAR( register1[reg_resolution[operand[i][0]] ] );
        }
        else if(strcmp(proc[i],"SHIFTL")==0){
            SHIFTL( register1[reg_resolution[operand[i][0]] ], operand[i] );
        }
        else if(strcmp(proc[i],"SHIFTR")==0){
            SHIFTR( register1[reg_resolution[operand[i][0]] ], operand[i] );
        }

        else if(strcmp(proc[i],"ADDF")==0){
//            if(operand[i][2] == 'A'){
//                if(operand[i][0] == 'A'){
//                    float_acc = 2.0*float_acc;
//                }
//                else if(operand[i][0] == 'B'){
//                    float_acc += float_b;
//                }
//                else if(operand[i][0] == 'C'){
//                    float_acc += float_c;
//                }
//            }
//            else if(operand[i][2] == 'B'){
//                if(operand[i][0] == 'A'){
//                    float_b += float_acc;
//                }
//                else if(operand[i][0] == 'B'){
//                     float_b = 2.0*float_b;
//                }
//                else if(operand[i][0] == 'C'){
//                    float_b += float_c;
//                }
//            }
//            else if(operand[i][2] == 'C'){
//                if(operand[i][0] == 'A'){
//                    float_c += float_acc;
//                }
//                else if(operand[i][0] == 'B'){
//                     float_c += float_b;
//                }
//                else if(operand[i][0] == 'C'){
//                    float_c = 2*float_c;
//                }
//            }
            float_acc = 1.0*register1[reg_resolution['A']]+register1[reg_resolution[operand[i][0]]];

        }

        else if(strcmp(proc[i],"SUBF")==0){
//            if(operand[i][2] == 'A'){
//                if(operand[i][0] == 'A'){
//                    float_acc = 0.0;
//                }
//                else if(operand[i][0] == 'B'){
//                    float_acc -= float_b;
//                }
//                else if(operand[i][0] == 'C'){
//                    float_acc -= float_c;
//                }
//            }
//            else if(operand[i][2] == 'B'){
//                if(operand[i][0] == 'A'){
//                    float_b -= float_acc;
//                }
//                else if(operand[i][0] == 'B'){
//                     float_b = 0.0;
//                }
//                else if(operand[i][0] == 'C'){
//                    float_b -= float_c;
//                }
//            }
//            else if(operand[i][2] == 'C'){
//                if(operand[i][0] == 'A'){
//                    float_c -= float_acc;
//                }
//                else if(operand[i][0] == 'B'){
//                     float_c -= float_b;
//                }
//                else if(operand[i][0] == 'C'){
//                    float_c = 0.0;
//                }
//            }
              float_acc = 1.0*register1[reg_resolution['A']]-register1[reg_resolution[operand[i][0]]];
        }

        else if(strcmp(proc[i],"MULF")==0){
            float_acc = 1.0*register1[reg_resolution['A']]*register1[reg_resolution[operand[i][0]]];
        }

        else if(strcmp(proc[i],"DIVF")==0){
            float_acc = 1.0*register1[reg_resolution['A']]/register1[reg_resolution[operand[i][0]]];
        }

        i++;
    }
}


/*--------------------main function to link all the memory and registers with the gui ----------------------------------*/

void assem::link_gui()
{
  char** proc;
  char** operand;
  opn_list=new char*[100];
  proc=new char*[100];
  operand=new char*[100];
  func_list=new char*[100];
  int value_list[1000];
  int indo=0;
  int length=0;
  int i=0,j=0,m[10],len,k,l=0;

  /*opening all the necessary files*/

  reg_res_init();
  open_all_files();
  fscanf(f1,"%s%s%s",la,mne,opnd);
  if(strcmp(mne,"START")==0)
 {
    sa=atoi(opnd);
    strcpy(name,la);
    lc=sa;
 }
 store_lc=lc;
  //error handing to see if start is there at the beginning
 strcpy(s1,"*");
 fscanf(f1,"%s%s%s",la,mne,opnd);
 while(strcmp(mne,"END")!=0){

     if(strcmp(la,"-")==0){
        fscanf(f2,"%s%s",mne1,opnd1);
        while(!feof(f2)){
            //to check if the opcode is not in the file
            if(strcmp(mne1,mne)==0){
                m[i]=lc+1;
                if(strcmp(opnd,"T")==0){
                    fprintf(f4,"%s\t%s\n",opnd,"1003");
                }
                else if(strcmp(opnd,"A")==0){
                    fprintf(f4,"%s\t%s\n",opnd,"1001");
                }
                else if(strcmp(opnd,"S")==0){
                    fprintf(f4,"%s\t%s\n",opnd,"1004");
                }
                else if(strcmp(opnd,"X")==0){
                    fprintf(f4,"%s\t%s\n",opnd,"1002");
                }
                else if(strcmp(opnd,"B")==0){
                    fprintf(f4,"%s\t%s\n",opnd,"1005");
                }
                else
                    fprintf(f3,"%s\t%s\n",opnd,s1);
                fprintf(f5,"%s\t0000\n",opnd1);
                lc=lc+3;
                i=i+1;
                proc[length]=new char[10];
                operand[length]=new char[10];
                int r=0;
                for(r=0;mne1[r]!='\0';r++) {
                    proc[length][r]=mne1[r];
                }
                proc[length][r]='\0';
                for(r=0;opnd[r]!='\0';r++) {			//modified! mne1 to opnd
                    operand[length][r]=opnd[r];
                }
                operand[length][r]='\0';
                length++;
                break;
            }
            else
                fscanf(f2,"%s%s",mne1,opnd1);
        }
    }
    else if(strcmp(mne,":")==0){
        func_list[indo]=new char[10];
        int r=0;
        while(la[r]!='\0') {
            func_list[indo][r]=la[r];
            r++;
        }
        func_list[indo][r]='\0';													// modified func_list[r] to modified func_list[indo][r] ; @aditya
        value_list[indo]=length;
        value_list1[indo]=length;
        printf("LABEL : %s & %d\n",func_list[indo],value_list[indo]);
        indo++;
    }
    else
    {
        if(flag_lc==0) {
            flag_lc=1;
            store_final_lc=lc;
        }
        fseek(f3, 0, SEEK_SET);
        fscanf(f3,"%s%s",sym,symaddr);
        snprintf(lcs, 10, "%d", lc);
        fprintf(f4,"%s\t%s\n",la,lcs);
        // while(!feof(f3)){
        //     if(strcmp(sym,la)==0){
        //         //itoa(lc,lcs,10);
        //         snprintf(lcs, 10, "%d", lc);
        //         fprintf(f4,"%s\t%s\n",la,lcs);
        //         //itoa(m[j],ms,10);
        //         snprintf(ms, 10, "%d", m[j]);
        //         j=j+1;
        //         fprintf(f5,"%s\t%s\n",ms,lcs);
        //         i=i+1;
        //         break;
        //     }
        //     else
        //         fscanf(f3,"%s%s",sym,symaddr);
        // }
        if(mne[0]=='R'&&mne[3]=='W') {
            vals_list[indu]=lc;
            lc=lc+3*atoi(opnd);
            opn_list[indu]=new char[20];
            int r=0;
            while(la[r]!='\0') {
                opn_list[indu][r]=la[r];
                r++;
            }
            opn_list[indu][r]='\0';

            indu++;
        }
        else if(strcmp(mne,"BYTE")==0){
            strcpy(s2,"-");
            len=strlen(opnd);
            lc=lc+len-2;
            for(k=2;k<len;k++){
                q[l]=opnd[k];
                l=l+1;
            }
            fprintf(f5,"%s\t%s\n",q,s2);
            break;
        }
        else if(strcmp(mne,"RESB")==0){
            vals_list[indu]=lc;
            lc=lc+atoi(opnd);
            opn_list[indu]=new char[10];
            int r=0;
            while(la[r]!='\0') {
                opn_list[indu][r]=la[r];
                r++;
            }
            opn_list[indu][r]='\0';

            indu++;
        }
        else if(strcmp(mne,"WORD")==0){
            vals_list[indu]=lc;
            memory.storeWord(lc,atoi(opnd));
            //strcpy(s3,"#");
            lc=lc+3;
            opn_list[indu]=new char[10];
            int r=0;
            while(la[r]!='\0') {
                opn_list[indu][r]=la[r];
                r++;
            }
            opn_list[indu][r]='\0';
//		printf("%s and %s\n",mne,opn_list[indu]);
        //    vals_list[indu]=lc;
            indu++;
            fprintf(f5,"%s\t%s\n",opnd,s3);
        }
    }
    fseek(f2,0,SEEK_SET);
    fscanf(f1,"%s%s%s",la,mne,opnd);
    if(mne[0]=='E')
        break;
  }
  final_pc = lc;
 fclose(f1);
 fclose(f2);
 fclose(f3);
 fclose(f4);
 fclose(f5);

 proc12=proc;
 operand12=operand;
 length1=length;
 register1[reg_resolution['P'] ]=final_pc;
 printf("%d is pc\n",final_pc);
 execute(proc,operand,length,func_list,value_list);

 object_code_generation();

}

int assem::find_add_opn(char *opn) {
    char tempo[100];
    int i,ln,index_flag=0;
    for(i=0;opn[i]!='\0';i++) {
        if(opn[i]!=',')
            tempo[i]=opn[i];
        else {
            index_flag=1;
            tempo[i]='\0';
            break;
        }
    }
    tempo[i]='\0';
    for(i=0;i<indu;i++) {
        if(strcmp(opn_list[i],tempo)==0) {
            if(index_flag==1) {
                return vals_list[i]+register1[reg_resolution['X']];
            }
            return vals_list[i];
        }
    }
 //   printf("%s is operand\n\n",opn);
    return 0;
}


/*-------------------generating the object code-----------------------------------------------------*/

void assem::object_code_generation(){

    /*					LETS START NOW                      */
    int diff=store_final_lc-store_lc;
    int final_lc=store_final_lc;
    lc = store_lc;

    f1=fopen(INPUT_FILE_NAME, "rw+");
    f2=fopen("optab.txt","r");
    f7 = fopen("object.txt","w+");

      fseek(f1,0,SEEK_SET);
      fscanf(f1,"%s%s%s",la,mne,opnd);
      fprintf(f7,"H^%s^%d^%d\n",la,store_lc,diff);
//      printf("H^%s^%d^%d\n",la,store_lc,diff);
      fscanf(f1,"%s%s%s",la,mne,opnd);
      int count_text=0;
      while(la[0]=='-' || mne[0]==':') {
	int j,split=0;
          char n[10];
          for(j=0;opnd[j]!='\0';j++) {
              if(opnd[j]==',') {
                  split=1;
                  n[j]='\0';
                  break;
              }
              n[j]=opnd[j];
          }
        //printf("%s %s\n",mne,opnd);
        if(la[0]=='-') {
        f2=fopen("optab.txt","r");
        fseek(f2,0,SEEK_SET);
        int opcode;
        while(!feof(f2)) {
            fscanf(f2,"%s%s",mne1,opnd1);
            if(strcmp(mne,mne1)==0) opcode = atoi(opnd1);
        }
        if(count_text==0) {
            if(final_lc-lc>=30) {
                fprintf(f7,"T^%d^%d^",lc,30);
  //              printf("T^%d^%d^",lc,30);
            }
            else {
                fprintf(f7,"T^%d^%d^",lc,final_lc-lc);
    //            printf("T^%d^%d^",lc,final_lc-lc);
            }
        }
        int add_opn;
        if(split==0) {
 //           printf("yo\n");
            add_opn=find_add_opn(opnd);
            if(opnd[0]=='T' && opnd[1]=='\0') add_opn=1003;
            else if(opnd[0]=='A' && opnd[1]=='\0') add_opn=1001;
            else if(opnd[0]=='B' && opnd[1]=='\0') add_opn=1004;
            else if(opnd[0]=='X' && opnd[1]=='\0') add_opn=1002;
   //        else if(opnd[0]=='A' && opnd[1]=='\0') add_opn=1001;
        }
        else {
            char b[2];
            b[0]=opnd[j-1];
            b[1]='\0';
           // printf("%s is name %s is b\n",n,b);
            add_opn=find_add_opn(n)+find_add_opn(b)+1000+register1[reg_resolution[opnd[j+1]]];
           // printf("%d is address of n\n",find_add_opn(b));
        }
        fprintf(f7,"%d%d",opcode,add_opn);
        memory.storeByte(lc,opcode);
        memory.storeByte(lc+1,add_opn/100);
        memory.storeByte(lc+2,add_opn%100);
      //  printf("%s-----%s\n",mne,opnd);
  //      printf("%d%d",opcode,add_opn);
        if(count_text<9 && final_lc - lc-3 > 0) {
            fprintf(f7,"^");
    //        printf("^");
        }
        count_text++;
        if(count_text==10 || final_lc-lc-3==0){
             fprintf(f7,"\n");
      //       printf("\n");
             count_text=0;
        }
        lc+=3;
        }
        fscanf(f1,"%s%s%s",la,mne,opnd);
        }
        fprintf(f7,"E^%d\n",store_lc);
        //printf("E^%d\n",store_lc);

}




/*---------------------when button debug gets clicked---------------------------------------*/

void assem::on_pushButton_2_clicked()
{
   ui->pushButton_5->setEnabled(true);
   if(::count==0)
       ui->textEdit->clear();
   ::count = 1;
    clear_reg();
    reg_status();
    position = 1;

}



/*--------------------------when the button next gets clicked-------------------------------------*/
void assem::on_pushButton_5_clicked()
{
   printf("POSITION : %d\n",position);
   QTextCursor cursor( ui->textEdit->textCursor() );
   QTextCharFormat format;
   int count1 = 0;
   format.setFontWeight( QFont::DemiBold );
   format.setForeground( QBrush( QColor( "red" ) ) );
   cursor.setCharFormat( format );
   fstream file(INPUT_FILE_NAME);
   string line8;
   for (int i = 1; i <=position; i++){
           std::getline(file, line8);
    }
   QString qstr = QString::fromStdString(line8);
    cursor.insertText(qstr);
    cursor.insertText("\n");

   position++;
   file.close();
   int po=store_lc+(position-2)*3;
   register1[reg_resolution['P']]=po;
   execute1(proc12,operand12,length1,func_list,value_list1,position);
}

int state_univ;
/*------------------------------to clear regsiters to initial values----------------------------------------*/
void assem::clear_reg(){

    register1[reg_resolution['A']]   = 0;
    register1[ reg_resolution['X'] ] = 0;
    register1[ reg_resolution['L'] ] = 0;
    register1[ reg_resolution['B'] ] = 0;
    register1[ reg_resolution['S'] ] = 0;
    register1[ reg_resolution['T'] ] = 0;
    register1[ reg_resolution['F'] ] = 0;
    float_acc = 0.0;
    register1[ reg_resolution['W']]  = 0;
}


/*----------------------------to execute each instruction line by line----------------------------------*/
void assem::execute1(char** proc,char** operand,int length,char** func_list,int* value_list,int h) {


    int i=position-2;
	if(i<length && i>=0 ) {
        printf("%s and %s\n",proc[i],operand[i]);
        if(strcmp(proc[i],"MOD")==0) {
            MOD(register1[ reg_resolution['A'] ], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"RD")==0) {
            int v=0;

           if(q_rd.empty()==false){
                v = q_rd.front();
                q_rd.pop();
            }

            RD(v,register1[ reg_resolution['A'] ]);
            printf("%d is the input\n",v);

        }
        else if(strcmp(proc[i],"WD")==0) {
            //to be implemented
        }

        /*new functions*/

        else if(strncmp(proc[i], "ST", 2)==0) {     //contains ST(reg) and STCH(as well)
                    int loop=0;
                    int split=0;
                    char name[20];
                    for(loop=0;operand[i][loop]!='\0';loop++) {
                        if(operand[i][loop]==','){

                            name[loop]='\0';
                            split=1;
                            break;
                        }
                        name[loop]=operand[i][loop];
                    }
                    name[loop]='\0';
                    if(split==0) {
                        int opera;
                        opera = find_add(operand[i]);
                        int reg_value;
                        if(strcmp(proc[i], "STCH") != 0){
                            ST( register1[ reg_resolution[proc[i][2]] ], reg_value );
                            memory.storeWord(vals_list[opera], reg_value);
                        }
                        else{
                            ST( register1[ reg_resolution['A'] ], reg_value );
                            memory.storeByte(vals_list[opera], reg_value);
                        }
                    }
                    else {
                        int opera=find_add(name);
                        int reg_value, memloc;
                        memloc = vals_list[opera]+ 3*register1[ reg_resolution['X'] ];
//                      cout <<"vallsit->" << vals_list[opera] << "   ||||    " << register1[ reg_resolution['X'] ] <<"\t";
//                      cout<<"MEMORY : _________________"<<memloc<<"\n";
                        if(strcmp(proc[i], "STCH") != 0){
                            ST( register1[ reg_resolution[proc[i][2]] ], reg_value );
                            memory.storeWord(memloc, reg_value);
                        }
                        else{
                            ST( register1[ reg_resolution['A'] ], reg_value );
                            memory.storeByte(memloc, reg_value);
                        }
                    }
        }
        else if(strncmp(proc[i], "LD", 2) == 0) {   //contains LD(reg) and LDCH(as well)
                    int loop=0;
                    int split=0;
                    char name[20];
                    for(loop=0;operand[i][loop]!='\0';loop++) {
                        if(operand[i][loop]==','){
                            name[loop]='\0';
                            split=1;
                            break;
                        }
                        name[loop]=operand[i][loop];
                    }
                    name[loop]='\0';
                    if(split==0) {
                        int opera=find_val(operand[i]);
                //        cout<<"LOADING MEMORY in reg "<< proc[i][2] <<" IS "<< opera <<"\n";
                        if(strcmp(proc[i], "LDCH") != 0){
                            LD(register1[reg_resolution[proc[i][2]]],opera);
                        }
                        else{
                            LD(register1[reg_resolution['A']],(opera & 0xFF) );
                        }

                    }
                    else {
                        int opera=find_add(name);
                        int memloc = vals_list[opera] + 3*register1[ reg_resolution['X'] ];
                        if(strcmp(proc[i], "LDCH") != 0){
                            LD(register1[ reg_resolution[proc[i][2]] ], memory.loadWord( memloc ));
                        }
                        else{
                            LD(register1[reg_resolution['A']],memory.loadByte(memloc) );
                        }
                    }
        }
        else if(strcmp(proc[i],"ADD")==0){
            ADD(register1[reg_resolution['A']], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"SUB")==0){
            SUB(register1[reg_resolution['A']], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"SUBR")==0){
            SUB( register1[ reg_resolution[operand[i][2]] ], register1[ reg_resolution[operand[i][0]] ] );
        }
        else if(strcmp(proc[i],"ADDR")==0){
            ADD( register1[ reg_resolution[operand[i][2]] ], register1[ reg_resolution[operand[i][0]] ] );			//ADDR A, S means Add A to S and store it in S.
        }
        else if(strcmp(proc[i],"COMP")==0){
            state_univ = COMP( register1[ reg_resolution['A'] ], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"COMPR")==0){
            state_univ = COMP( register1[ reg_resolution[operand[i][0]] ], register1[ reg_resolution[operand[i][2]] ] );
        }
        else if(strcmp(proc[i],"DIV")==0){
            DIV(register1[reg_resolution['A']], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"DIVR")==0){
            DIV( register1[ reg_resolution[operand[i][2]] ], register1[ reg_resolution[operand[i][0]] ] );
        }
        else if(strcmp(proc[i],"MUL")==0){
            MUL(register1[reg_resolution['A']], find_val(operand[i]) );
        }
        else if(strcmp(proc[i],"MULR")==0){
            MUL( register1[ reg_resolution[operand[i][2]] ], register1[ reg_resolution[operand[i][0]] ] );
        }
        else if(strcmp(proc[i],"JLT")==0) {
            int id=0;
            id=search(operand[i],func_list);
            if(state_univ<0)
                i=value_list[id]-1;														//	printf("%d is acc\n",register1[0]);
                printf("state is %d --- %d jump is i and equi is %d\n",state_univ,value_list[id],i+1);
           //     printf("JUMPS TO %s %s\n",proc[i+1],)
                //	printf("%d is acc\n",register1[0]);
    //            cout << "IN JLT :: " << i << " <-- i ::: id -> :::: " << id <<"\n";
        }
        else if(strcmp(proc[i],"JEQ")==0) {
            int id=0;
            id=search(operand[i],func_list);
            if(state_univ==0)
            i=value_list[id]-1;
        }
        else if(strcmp(proc[i],"JGT")==0) {
            int id=0;
            id=search(operand[i],func_list);
            if(state_univ>0)
            i=value_list[id]-1;
        }
        else if(strcmp(proc[i],"JSUB")==0) {
            int id=0;
            id=search(operand[i],func_list);
            i=value_list[id]-1;
            register1[ reg_resolution['L'] ] = register1[ reg_resolution['P'] ];
            register1[ reg_resolution['P'] ] = find_val(operand[i]);
        }
        else if(strcmp(proc[i],"RSUB")==0) {
            int id=0;
            id=search(operand[i],func_list);
            register1[ reg_resolution['P'] ] = register1[ reg_resolution['L'] ];
        }
        else if(strcmp(proc[i],"TIXR")==0){
            printf("%d is the T reg value\n",register1[ reg_resolution[operand[i][0]] ]);
            state_univ=TIXR(register1[ reg_resolution[operand[i][0]] ], register1[reg_resolution['X']]);
        }
        else if(strcmp(proc[i],"TIX")==0){
            state_univ=TIXR( find_val(operand[i]) , register1[reg_resolution['X']] );
        }
        else if(strcmp(proc[i],"OR")==0){
            OR( register1[reg_resolution['A']],  find_val(operand[i])  );
        }
        else if(strcmp(proc[i],"AND")==0){
            AND( register1[reg_resolution['A']],  find_val(operand[i])  );
        }
        else if(strcmp(proc[i],"CLEAR")==0){
            CLEAR( register1[reg_resolution[operand[i][0]] ] );
        }
        else if(strcmp(proc[i],"SHIFTL")==0){
            SHIFTL( register1[reg_resolution[operand[i][0]] ], operand[i] );
        }
        else if(strcmp(proc[i],"SHIFTR")==0){
            SHIFTR( register1[reg_resolution[operand[i][0]] ], operand[i] );
        }

      //  if(i==h){
		  mem_status();
            reg_status();
            position=i+2;

       // }
       // i++;
    }
}




/*-----------------------------to browse and load a file------------------------------------------------------*/

void assem::on_pushButton_4_clicked()
{
   //QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),"/home", tr("Images (*.png *.xpm *.jpg)"));
   QStringList fileNames = QFileDialog::getOpenFileNames(this, tr("Open File"),"/path/to/file/",tr("All Files (*.*)"));
   ui->listWidget->addItems(fileNames);
   QString fileName = fileNames.join("");
   QFile file5(fileName);
   file5.open(QIODevice::ReadOnly);
   QTextStream stream(&file5);
   QString content = stream.readAll();
   file5.close();
   ui->textEdit->setText(content);



}

void assem::on_pushButton_6_clicked()
{
    error_flag = 0;
    position = 1;
    ::count = 0;
    flag = 0;
    indu = 0;
    if(error_flag!=0)
            ui->textEdit_2->setText("compile error");
    else{

        gen_sym_tab();

    }
}


void assem::on_pushButton_7_clicked()
{
    error_flag = 0;
    position = 1;
    ::count = 0;
    flag = 0;
    indu = 0;
    if(error_flag!=0)
            ui->textEdit_2->setText("compile error");
    else{


        gen_ob_code();


    }

}

void assem::on_pushButton_8_clicked()
{
    uij = new conversion_window(this);
    uij->show();

}
