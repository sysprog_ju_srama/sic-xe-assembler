#include "conversion_window.h"
#include "ui_conversion_window.h"
#include "hexdecinterconversion.cpp"
#include<string>
using namespace std;
conversion_window::conversion_window(QWidget *parent) :
    QMainWindow(parent),
    uij(new Ui::conversion_window)
{
    uij->setupUi(this);
}

conversion_window::~conversion_window()
{
    delete uij;
}

void conversion_window::convert_to_hex(){

    QString g = uij->textEdit_2->toPlainText();
    string h = g.toStdString();
    InterConversion test1(h, 'd');
    test1.convert();
    string k = test1.getHex();
    //cout<<k<<endl;
    QString qstr = QString::fromStdString(k);
    uij->textEdit->setText(qstr);

}

void conversion_window::convert_to_dec(){

   //uij->textEdit->clear();
   QString g = uij->textEdit->toPlainText();
    string h = g.toStdString();
    InterConversion test1(h, 'x');
    test1.convert();
    string k = test1.getDec();
    //cout<<k<<endl;
    QString qstr = QString::fromStdString(k);
    uij->textEdit_2->setText(qstr);

}


void conversion_window::on_pushButton_2_clicked()
{
    convert_to_hex();
}

void conversion_window::on_pushButton_clicked()
{
    convert_to_dec();
}
