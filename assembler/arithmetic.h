/*all the functionalities used by the assembler*/
#ifndef ARITHMATIC_H
#define ARITHMATIC_H
#include <cstdlib>

int TIXR(int reg1,int &regX) {
    regX += 1;
    int p = regX-reg1;
    if(p>0) return 1;
    else if(p==0) return 0;
    return -1;
}
void LD(int &a,int val) {
    a=val;
}
void ST(int &a,int &result) {
    result=a;
}
void ADD(int &a,int no) {
    a=a+no;
}
void SUB(int &a,int no) {
    a=a-no;
}
/*	void SUBR(int &a,int no) {
    a=a-no;
}
void ADDR(int &a,int no) {
    a=a+no;
}
*/	void MUL(int &a,int no) {
    a=a*no;
}
void DIV(int &a,int no) {
    a=a/no;
}
void RD(int v,int &b) {

    b=v;
}
void MOD(int &a,int no) {
    a=a%no;
}
void JLT(int value,int& state) {
    if(value<0) state=0;
}
int COMP(int a,int b) {
    if(a<b) return -1;
    else if(a==b) return 0;
    return 1;
}
void OR(int &acc, int num) {
    acc |= num;
}
void AND(int &acc, int num) {
    acc &= num;
}
void CLEAR(int &reg){
    reg = 0;
}
void SHIFTL(int &reg, char* n_string) {
    int n = atoi(n_string);
    reg = (reg << n ) | ( reg >> (32-n) );
}
void SHIFTR(int &reg, char* n_string) {
    int n = atoi(n_string);
    reg >>= n;
}






#endif // ARITHMATIC_H
