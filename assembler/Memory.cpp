#include <stdint.h>
using namespace std;

class Memory{
    int SIZE;
    uint8_t *memory;

    bool isMemLocOkay(int memloc){
        return (memloc >= 0 && memloc < SIZE - 2);
    }

public:
    Memory(int size = 0){
        if(size == 0){
            size = 1;
            size <<= 20;
        }
        SIZE = size;
        memory = new uint8_t[SIZE];
    }
    void setSize(int size){
        SIZE = size;
    }
    int getSize(void){
        return SIZE;
    }
    void storeWord(int memloc, int word){
        if(isMemLocOkay(memloc)){
            memory[memloc++] = word&0xFF;
            word >>= 8;
            memory[memloc++] = word&0xFF;
            word >>= 8;
            memory[memloc++] = word&0xFF;
        }
    }

    int loadWord(int memloc){
        if(isMemLocOkay(memloc)){
            int word = 0;
            for(int i = memloc+2; i >= memloc; i--){
                word <<= 8;
                word += memory[i];
            }
            return word;
        }
        return 0x00;
    }

    int loadByte(int memloc){
        if(isMemLocOkay(memloc)){
            return memory[memloc];
        }
        else{
            return 0xFF;
        }
    }

    void storeByte(int memloc, int byte){
        if(isMemLocOkay(memloc)){
            memory[memloc] = uint8_t(byte&0xFF);
        }
    }

    ~Memory(){
        delete[]  memory;
    }
};
