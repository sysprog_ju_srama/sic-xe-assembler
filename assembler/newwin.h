#ifndef NEWWIN_H
#define NEWWIN_H

#include <QMainWindow>
#include<QString>
namespace Ui {
class newwin;
}

class newwin : public QMainWindow
{
    Q_OBJECT

public:
    explicit newwin(QWidget *parent = 0);
    QString s;
    void getstring(QString);
    ~newwin();

public:
    Ui::newwin *uip;
};

#endif // NEWWIN_H
