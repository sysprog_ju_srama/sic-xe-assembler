#ifndef CONVERSION_WINDOW_H
#define CONVERSION_WINDOW_H

#include <QMainWindow>

namespace Ui {
class conversion_window;
}

class conversion_window : public QMainWindow
{
    Q_OBJECT

public:
    explicit conversion_window(QWidget *parent = 0);
    ~conversion_window();
    void convert_to_hex();
    void convert_to_dec();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::conversion_window *uij;
};

#endif // CONVERSION_WINDOW_H
