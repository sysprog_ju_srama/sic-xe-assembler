#include <string>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <cstdlib>

class InterConversion{

    std::string bin, hex, dec;
    char radix;

    void make_binary(void){
        long long i_dec, i_hex;
        int i, temp;
        switch(radix){
            case 'd':
                i_dec = atoi(dec.c_str());
                i = 0;
                while(i_dec){
                    bin[i++] = (i_dec&1)+'0';
                    i_dec >>= 1;
                }
                std::reverse(bin.begin(), bin.begin()+i);
                break;

            case 'x':
                long long i_hex = 0, len;
                for(len = hex.size()-1; hex[len] == '\0'; len--)
                    ;
                std::cout << len <<"!!\n";
                std::string str(hex);
                std::reverse(str.begin(),str.begin()+len+1 );
                for(i=0; i <= len; i++){
                    i_hex *= 16;
                    i_hex += decValue(str[i]);
                }
                i = 0;
                while(len-- >= 0){
                    temp = i_hex&15;
                    bin[i++] = ((temp & 0x8)>>3)+'0';
                    bin[i++] = ((temp & 0x4)>>2)+'0';
                    bin[i++] = ((temp & 0x2)>>1)+'0';
                    bin[i++] = (temp & 0x1)+'0';
                    i_hex >>= 4;
                }
                break;
        }
    }

    int decValue(char ch){
        if('A' <= ch && ch <= 'F'){
            return ch - 'A'+10;
        }
        if('a' <= ch && ch <= 'f' ){
            return ch - 'a'+10;
        }
        return ch - '0';
    }

    void make_hex(void){
        int hex_num, _hex_index = 0, i;
        for(i = 0; i <= bin.size()-1 && bin[i] != '\0'; i++)
            ;
        i--;
        for(; i >= 0; ){
            hex_num = 0;
            int j;
            for(j = 0; j<4 && i >= 0; j++, i--){
                hex_num += ((bin[i] - '0')<<j);
            }
            if(hex_num >= 10){
                switch (hex_num) {
                    case 10:
                        hex[_hex_index++] = 'A';
                        break;
                    case 11:
                        hex[_hex_index++] = 'B';
                        break;
                    case 12:
                        hex[_hex_index++] = 'C';
                        break;
                    case 13:
                        hex[_hex_index++] = 'D';
                        break;
                    case 14:
                        hex[_hex_index++] = 'E';
                        break;
                    case 15:
                        hex[_hex_index++] = 'F';
                        break;
                }
            }
            else{
                hex[_hex_index++] = hex_num + '0';
            }
        }

        std::reverse(hex.begin(), hex.begin()+_hex_index);
    }

    void make_dec(void){
        int _dec_len, i, j;
        long long num;
        num = 0;
        for(i = 0; i < bin.size() && bin[i] != '\0'; i++){
            num <<= 1;
            num += (bin[i]-'0');
        }
        _dec_len = log10(num);
        for(i = 0; i <= _dec_len; i++){
            dec[i] = (num%10)+'0';
            num /= 10;
        }
        std::reverse(dec.begin(), dec.begin() + _dec_len+1);

    }

public:

    InterConversion(){
        bin.assign(66, '\0');
        dec.assign(20, '\0');
        hex.assign(18, '\0');
    }

    InterConversion(std::string str, char radix){
        bin.assign(66, '\0');
        dec.assign(20, '\0');
        hex.assign(18, '\0');
        this->radix = radix;
        if(radix == 'b')
            bin = std::string(str);
        else if(radix == 'd')
            dec = std::string(str);
        else if(radix == 'x')
            hex = std::string(str);
    }

    void convert(void){
        if(radix != 'b'){
            make_binary();
        }
        std::cout << "binary " << bin << "!\n";
        if(radix != 'x'){
            make_hex();
        }
        std::cout << "hex " << hex << "!\n";
        if(radix != 'd'){
            make_dec();
        }
        std::cout << "dec" << dec << "!\n";
    }

    std::string getBin(){
        return bin;
    }

    std::string getHex(){
        //std::cout << hex << " in hex fn.\n";
        return hex;
    }

    std::string getDec(){
        //std::cout << dec << " in dec fn.\n";
        return dec;
    }

    void setDec(std::string str){
        dec = std::string(str);
        radix = 'd';
    }

    void setHex(std::string str){
        hex = std::string(str);
        radix = 'x';
    }

    void setBin(std::string str){
        bin = std::string(str);
        radix = 'b';
    }


};
