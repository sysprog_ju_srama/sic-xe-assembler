#ifndef INPUT_WINDOW_H
#define INPUT_WINDOW_H

#include <QMainWindow>

namespace Ui {
class input_window;
}

class input_window : public QMainWindow
{
    Q_OBJECT

public:
    explicit input_window(QWidget *parent = 0);
    ~input_window();
    QString getstring();
    bool input_taken();

private slots:
    void on_pushButton_clicked();


    //void on_pushButton_clicked(bool checked);

private:
    Ui::input_window *uik;
    //bool input_flag;

};

#endif // INPUT_WINDOW_H
