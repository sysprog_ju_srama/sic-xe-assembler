#include "input_window.h"
#include "ui_input_window.h"

input_window::input_window(QWidget *parent) :
    QMainWindow(parent),
    uik(new Ui::input_window)
{
    uik->setupUi(this);

}

input_window::~input_window()
{
    delete uik;
}

QString input_window::getstring(){

    QString p = uik->textEdit->toPlainText();
    return p;
}

void input_window::on_pushButton_clicked()
{

    this->close();

}
