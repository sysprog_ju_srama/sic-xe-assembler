#ifndef ASSEM_H
#define ASSEM_H

#include <QMainWindow>
#include "newwin.h"
#include "symbol_table.h"
#include "input_window.h"
#include "conversion_window.h"
#include<string>

using namespace std;
namespace Ui {
class assem;
}

class assem : public QMainWindow
{
    Q_OBJECT

public:
    explicit assem(QWidget *parent = 0);
    ~assem();

private slots:
    void on_BUILD_clicked();

    void on_pushButton_clicked();

    void link_gui();

    int find_add(char *);

    int find_val(char *);

    int search(char *,char **);

    void execute(char**,char** ,int,char**,int* );

    void execute1(char** ,char** ,int ,char** ,int* ,int );

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void mem_status();

    int find_add_opn(char *);

    void clear_reg();

    void reg_status();

    void gen_ob_code();

    void object_code_generation();

    void gen_sym_tab();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void error_handle();

    void on_pushButton_8_clicked();

    void take_input();

    bool check_if_integer(char*);

private:
   Ui::assem *ui;
   newwin *uip;
   symbol_table *uiz;
   input_window *uik;
   conversion_window *uij;


};

#endif // ASSEM_H
