#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

#include <QMainWindow>
#include<QString>
namespace Ui {
class symbol_table;
}

class symbol_table : public QMainWindow
{
    Q_OBJECT

public:
    explicit symbol_table(QWidget *parent = 0);
    QString s;
    void getstring1(QString);
    ~symbol_table();

private:
    Ui::symbol_table *uiz;
};

#endif // SYMBOL_TABLE_H
