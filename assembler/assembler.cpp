#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <map>

using namespace std;
char** func_list;
FILE *f1,*f2,*f3,*f4,*f5,*f6;

int TIXR(int &reg1,int &reg2) {
    reg2+=1;
    int p=reg2-reg1;
    if(p>0) return 1;
    else if(p==0) return 0;
    return -1;
}
void LDA(int &a,int val) {
    a=val;
}
void STA(int &a,int &result) {
    result=a;
}
void ADD(int &a,int no) {
    a=a+no;
}
void SUB(int &a,int no) {
    a=a-no;
}
void SUBR(int &a,int no) {
    a=a-no;
}
void ADDR(int &a,int no) {
    a=a+no;
}
void MUL(int &a,int no) {
    a=a*no;
}
void DIV(int &a,int no) {
    a=a/no;
}
void MOD(int &a,int no) {
    a=a%no;
}
void JLT(int value,int& state) {
    if(value<0) state=0;
}
int COMPR(int a,int b) {
    if(a<b) return -1;
    else if(a==b) return 0;
    return 1;
}
int find_val(char *s) {
    fclose(f1);
    f1=fopen("input.txt","r");
    char a[10],b[10],c[10];
    fscanf(f1,"%s%s%s",a,b,c);
    while(strcmp(a,s)!=0) {
        fscanf(f1,"%s%s%s",a,b,c);
    }
    return atoi(c);
}
int search(char *p,char** func_list) {
    int i=0;
    while(strcmp(p,func_list[i])!=0){
        i++;
    }
    return i;
}
void execute(char** proc,char** operand,int length,int register1[],char** func_list,int* value_list) {
    int i=0;
    for(i=0;i<9;i++) register1[i]=0;
    i=0;
    int state=1;
    while(i<length) {
        if(strcmp(proc[i],"MOD")==0) {
            int opera=find_val(operand[i]);
            MOD(register1[0],opera);
        }
        if(strcmp(proc[i],"LDA")==0) {
            int opera=find_val(operand[i]);
            LDA(register1[0],opera);
        }
        if(strcmp(proc[i],"LDT")==0) {
            int opera=find_val(operand[i]);
            LDA(register1[5],opera);
        }
        if(strcmp(proc[i],"LDX")==0) {
            int opera=find_val(operand[i]);
            LDA(register1[1],opera);
        }
        if(strcmp(proc[i],"LDS")==0) {
            int opera=find_val(operand[i]);
            LDA(register1[4],opera);
        }
        if(strcmp(proc[i],"ADD")==0){
            int opera=find_val(operand[i]);
            ADD(register1[0],opera);
        }
        if(strcmp(proc[i],"SUB")==0){
            int opera=find_val(operand[i]);
            SUB(register1[0],opera);
        }
        if(strcmp(proc[i],"SUBR")==0){
            if(operand[i][0]=='A' &&operand[i][2]=='T' )
                SUBR(register1[0],register1[5]);
            if(operand[i][0]=='A' &&operand[i][2]=='X' )
                SUBR(register1[0],register1[1]);
            if(operand[i][0]=='X' &&operand[i][2]=='A' )
                SUBR(register1[1],register1[0]);
            if(operand[i][0]=='T' &&operand[i][2]=='A' )
                SUBR(register1[5],register1[0]);
            if(operand[i][0]=='T' &&operand[i][2]=='X' )
                SUBR(register1[5],register1[1]);
        }
        if(strcmp(proc[i],"ADDR")==0){
            if(operand[i][0]=='A' &&operand[i][2]=='T' )
                ADDR(register1[0],register1[5]);
            if(operand[i][0]=='A' &&operand[i][2]=='X' )
                ADDR(register1[0],register1[1]);
            if(operand[i][0]=='X' &&operand[i][2]=='A' )
                ADDR(register1[1],register1[0]);
            if(operand[i][0]=='T' &&operand[i][2]=='A' )
                ADDR(register1[5],register1[0]);
            if(operand[i][0]=='T' &&operand[i][2]=='X' )
                ADDR(register1[5],register1[1]);
        }
        if(strcmp(proc[i],"COMPR")==0){
            if(operand[i][0]=='A' &&operand[i][2]=='T' )
                state=COMPR(register1[0],register1[5]);
            if(operand[i][0]=='A' &&operand[i][2]=='X' )
                state=COMPR(register1[0],register1[1]);
            if(operand[i][0]=='X' &&operand[i][2]=='A' )
                state=COMPR(register1[1],register1[0]);
            if(operand[i][0]=='T' &&operand[i][2]=='A' )
                state=COMPR(register1[5],register1[0]);
                if(operand[i][0]=='T' &&operand[i][2]=='X' )
                state=COMPR(register1[5],register1[1]);
        }
        if(strcmp(proc[i],"DIV")==0){
            int opera=find_val(operand[i]);
            DIV(register1[0],opera);
        }
        if(strcmp(proc[i],"MUL")==0){
            int opera=find_val(operand[i]);
            MUL(register1[0],opera);
        }
        if(strcmp(proc[i],"JLT")==0) {
            int id=0;
            id=search(operand[i],func_list);
            if(state<0)
            i=value_list[id]-1;
        }
        if(strcmp(proc[i],"JEQ")==0) {
            int id=0;
            id=search(operand[i],func_list);
            if(state==0)
            i=value_list[id]-1;
        }
        if(strcmp(proc[i],"TIXR")==0){
            if(operand[i][0]=='T') {
                state=TIXR(register1[5],register1[1]);
            }
            if(operand[i][0]=='S') {
                state=TIXR(register1[4],register1[1]);
            }
        }
        i++;
    }
}
void register_state(int *register1) {
    printf("VALUE IN ACCUMULATOR %d\n",register1[0]);
    printf("VALUE IN REG X %d\n",register1[1]);
    printf("VALUE IN REG L %d\n",register1[2]);
    printf("VALUE IN REG B %d\n",register1[3]);
    printf("VALUE IN REG S %d\n",register1[4]);
    printf("VALUE IN REG T %d\n",register1[5]);
    printf("VALUE IN REG F %d\n",register1[6]);
    printf("VALUE IN REG PC %d\n",register1[7]);
}
int main()
{
  int register1[10];
  int mem[2000000];
  char** proc;
  char** operand;
  //char** func_list;
  proc=new char*[100];
  operand=new char*[100];
  func_list=new char*[100];
  int value_list[1000];
  int indo=0;
  int length=0;
  int lc,sa,i=0,j=0,m[10],pgmlen,len,k,len1,l=0;
  char name[10],opnd[10],la[10],mne[10],s1[10],mne1[10],opnd1[10];
  char lcs[10],ms[10];
  char sym[10],symaddr[10],obj1[10],obj2[10],s2[10],q[10],s3[10];
  int
  f1=fopen("input.txt","r");
  f2=fopen("optab.txt","r");
  f3=fopen("symtab.txt","w+");
  f4=fopen("symtab_final.txt","w+");
  f5=fopen("output.txt","w+");
  f6=fopen("register_status.txt","rw+");
  fscanf(f1,"%s%s%s",la,mne,opnd);
  if(strcmp(mne,"START")==0)
 {
    sa=atoi(opnd);
    strcpy(name,la);
    lc=sa;
 }
 strcpy(s1,"*");
 fscanf(f1,"%s%s%s",la,mne,opnd);
 while(strcmp(mne,"END")!=0)
 {
    fflush(stdin);
    fflush(stdout);
   if(strcmp(la,"-")==0)
   {

     fscanf(f2,"%s%s",mne1,opnd1);
     while(!feof(f2))
     {
       if(strcmp(mne1,mne)==0)
       {
    m[i]=lc+1;
    if(strcmp(opnd,"T")==0) {
     fprintf(f4,"%s\t%s\n",opnd,"1");
}
    else fprintf(f3,"%s\t%s\n",opnd,s1);
    char ab[10];
    fprintf(f5,"%s\t%s\n",opnd1,itoa(lc,ab,10));
    lc=lc+3;
    i=i+1;
    proc[length]=new char[10];
    operand[length]=new char[10];
    int r=0;
    for(r=0;mne1[r]!='\0';r++) {
        proc[length][r]=mne1[r];
    }
    proc[length][r]='\0';
    for(r=0;mne1[r]!='\0';r++) {
        operand[length][r]=opnd[r];
    }
    operand[length][r]='\0';
     length++;
    break;
       }
       else
     fscanf(f2,"%s%s",mne1,opnd1);
     }
   }
   else if(strcmp(mne,":")==0) {
        func_list[indo]=new char[10];
        int r=0;
        while(la[r]!='\0') {
            func_list[indo][r]=la[r];
            r++;
           }
        func_list[r]='\0';
        value_list[indo]=length;
  // 		 printf("%s & %d\n",func_list[indo],value_list[indo]);
        indo++;
   }
   else
   {
     fseek(f3,SEEK_SET,0);
     fscanf(f3,"%s%s",sym,symaddr);
     while(!feof(f3))
     {
       if(strcmp(sym,la)==0)
       {
    itoa(lc,lcs,10);
    fprintf(f4,"%s\t%s\n",la,lcs);
    itoa(m[j],ms,10);
    j=j+1;
    fprintf(f5,"%s\t%s\n",ms,lcs);
    i=i+1;
    break;
       }
       else
     fscanf(f3,"%s%s",sym,symaddr);
     }  //f3
     if(strcmp(mne,"RESW")==0)
      lc=lc+3*atoi(opnd);
     else if(strcmp(mne,"BYTE")==0)
     {
      strcpy(s2,"-");
      len=strlen(opnd);
      lc=lc+len-2;
      for(k=2;k<len;k++)
      {
      q[l]=opnd[k];
      l=l+1;
      }
      fprintf(f5,"%s\t%s\n",q,s2);
      break;
     }
     else if(strcmp(mne,"RESB")==0)
      lc=lc+atoi(opnd);
     else if(strcmp(mne,"WORD")==0)
     {
       strcpy(s3,"#");
       lc=lc+3;
       fprintf(f5,"%s\t%s\n",opnd,s3);
       break;
     }
    }    // else la=-


     fseek(f2,SEEK_SET,0);
     fscanf(f1,"%s%s%s",la,mne,opnd);
  }
  printf("     OBJECT CODE      \n\n");
  fclose(f1);
  fclose(f2);
  fclose(f3);
  fclose(f4);
  fseek(f5,SEEK_SET,0);
  pgmlen=lc-sa;
  printf("H^%s^%d^0%x\n",name,sa,pgmlen);
  printf("T^");
  printf("00%d^0%x",sa,pgmlen);
  fscanf(f5,"%s%s",obj1,obj2);

  while(!feof(f5))
  {

    if(strcmp(obj2,"-")==0)
      {
      printf("^");
      len1=strlen(obj1);
      for(k=0;k<len1;k++)
      printf("%d",obj1[k]);
      }
      else if(strcmp(obj2,"#")==0)
      {
       printf("^");
       printf("%s",obj1);
       }
       else if(strcmp(obj2,"0000")==0)
      printf("^%s%s",obj1,obj2);

    fscanf(f5,"%s%s ",obj1,obj2);
  }
  fseek(f5,SEEK_SET,0);
  fscanf(f5,"%s%s",obj1,obj2);
  while(!feof(f5))
  {
   if(strcmp(obj2,"0000")!=0)
   {
     if(strcmp(obj2,"-")!=0)
     {
     if(strcmp(obj2,"#")!=0)
     {
      printf("\n");
      printf("T^%s%s",obj1,obj2);
     }
    }
    }
    fscanf(f5,"%s%s",obj1,obj2);
   }
  printf("\nE^00%d\n",sa);
   printf("         */REGISTER STATUS */     \n\n");
 execute(proc,operand,length,register1,func_list,value_list);
 register1[7]=lc;
 register_state(register1);
 getch();
 return 0;
 }

 /*            END OF THE PROGRAM             */
