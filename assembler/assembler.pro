#-------------------------------------------------
#
# Project created by QtCreator 2016-09-30T12:58:11
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ass
TEMPLATE = app


SOURCES += main.cpp\
        assem.cpp \
    Memory.cpp \
    newwin.cpp \
    symbol_table.cpp \
    hexDecInterConversion.cpp \
    conversion_window.cpp \
    input_window.cpp

HEADERS  += assem.h \
    newwin.h \
    symbol_table.h \
    conversion_window.h \
    input_window.h \
    arithmetic.h

FORMS    += assem.ui \
    newwin.ui \
    symbol_table.ui \
    conversion_window.ui \
    input_window.ui
